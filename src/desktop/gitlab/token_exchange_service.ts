import assert from 'assert';
import { Account, OAuthAccount, serializeAccountSafe } from '../../common/platform/gitlab_account';
import { accountService, AccountService } from '../accounts/account_service';
import { log } from '../../common/log';
import { createExpiresTimestamp, GitLabService } from './gitlab_service';

/**
  We'll refresh the token 40s before it expires. On two hours it won't make a difference and
  I'd rather not rely on the local and server clocks being perfectly in sync. Otherwise we would
  risk making unauthorized requests.

  Originally we chose 10s, but 40s means that the sidebar refreshing (every 30s) will effectively
  ensure that we never have outdated token. This is important for the Language Server.
  The LS makes requests separately and it doesn't trigger the refreshing mechanism.

  Note: change this value to 7170 to simulate token expiration every 30s
*/
const SMALL_GRACE_DURATION_JUST_TO_BE_SURE = 40;

const needsRefresh = (account: OAuthAccount) => {
  const currentUnixTimestampInSeconds = Math.floor(new Date().getTime() / 1000);
  return (
    account.expiresAtTimestampInSeconds - SMALL_GRACE_DURATION_JUST_TO_BE_SURE <=
    currentUnixTimestampInSeconds
  );
};

export class TokenExchangeService {
  #accountService: AccountService;

  #refreshesInProgress: Record<string, Promise<Account> | undefined> = {};

  constructor(as = accountService) {
    this.#accountService = as;
  }

  async refreshIfNeeded(accountId: string): Promise<Account> {
    // before we start refreshing token, let's check if some other VS Code instance already has refreshed it
    await this.#accountService.reloadCache();
    const latestAccount = this.#accountService.getAccount(accountId);
    // this would happen if another VS Code Window deleted the account
    assert(latestAccount, `Account with id ${accountId} doesn't exist.`);
    if (latestAccount.type === 'token') return latestAccount;
    if (!needsRefresh(latestAccount)) {
      log.debug(`Using non-expired OAuth account ${latestAccount.id}.`);
      return latestAccount;
    }
    const refreshInProgress = this.#refreshesInProgress[accountId];
    if (refreshInProgress) return refreshInProgress;
    log.info(`Refreshing expired token for account ${latestAccount.id}.`);
    log.debug(`Refreshing expired token for account ${serializeAccountSafe(latestAccount)}.`);
    const refresh = this.#refreshToken(latestAccount).finally(() => {
      // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
      delete this.#refreshesInProgress[accountId];
    });
    this.#refreshesInProgress[accountId] = refresh;
    return refresh;
  }

  async #refreshToken(account: OAuthAccount): Promise<OAuthAccount> {
    const { instanceUrl, refreshToken } = account;
    const response = await GitLabService.exchangeToken({
      grantType: 'refresh_token',
      instanceUrl,
      refreshToken,
    });
    const refreshedAccount: OAuthAccount = {
      ...account,
      token: response.access_token,
      refreshToken: response.refresh_token,
      expiresAtTimestampInSeconds: createExpiresTimestamp(response),
    };
    await this.#accountService.updateAccountSecret(refreshedAccount);
    log.info(`Saved refreshed token for account ${refreshedAccount.id}.`);
    log.debug(`Saved refreshed token for account ${serializeAccountSafe(refreshedAccount)}.`);
    return refreshedAccount;
  }
}

export const tokenExchangeService = new TokenExchangeService();
