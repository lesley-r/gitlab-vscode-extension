export const GITLAB_COM_URL = 'https://gitlab.com';
export const CONFIG_NAMESPACE = 'gitlab';
export const DO_NOT_SHOW_CODE_SUGGESTIONS_VERSION_WARNING = 'DO_NOT_SHOW_VERSION_WARNING';
// NOTE: This needs to _always_ be a 3 digits
export const MINIMUM_CODE_SUGGESTIONS_VERSION = '16.8.0';

export const DO_NOT_SHOW_VERSION_WARNING = 'DO_NOT_SHOW_VERSION_WARNING';

// NOTE: This needs to _always_ be a 3 digits
export const MINIMUM_VERSION = '16.1.0';

export const REQUEST_TIMEOUT_MILLISECONDS = 25000;

// Webview IDs
export const DUO_WORKFLOW_WEBVIEW_ID = 'duo-workflow';
export const DUO_WORKFLOW_PANEL_WEBVIEW_ID = 'duo-workflow-panel';
export const DUO_CHAT_WEBVIEW_ID = 'duo-chat-v2';
export const SECURITY_VULNS_WEBVIEW_ID = 'security-vuln-details';

// Environment
// TODO: Remove process reference in common constants
// https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1615
export const IS_OSX = typeof process !== 'undefined' && process.platform === 'darwin';

export const LS_WEBVIEW_IDS = [
  DUO_WORKFLOW_WEBVIEW_ID,
  DUO_WORKFLOW_PANEL_WEBVIEW_ID,
  DUO_CHAT_WEBVIEW_ID,
];
