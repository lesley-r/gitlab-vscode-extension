import * as vscode from 'vscode';
import { createFakePartial } from '../../test_utils/create_fake_partial';
import { DUO_CHAT_WEBVIEW_ID } from '../../constants';
import { USER_COMMANDS } from '../../command_names';
import { LSDuoChatWebviewController } from './duo_chat_controller';

describe('LSDuoChatWebviewController', () => {
  const mockUrl = new URL('http://localhost');
  const mockTitle = 'duo-chat';
  const mockViewId = DUO_CHAT_WEBVIEW_ID;

  let controller: LSDuoChatWebviewController;
  let mockWebviewView: vscode.WebviewView;

  const mockExecuteCommand = jest
    .spyOn(vscode.commands, 'executeCommand')
    .mockResolvedValue(undefined);

  beforeEach(() => {
    controller = new LSDuoChatWebviewController({
      viewId: mockViewId,
      url: mockUrl,
      title: mockTitle,
    });

    mockWebviewView = createFakePartial<vscode.WebviewView>({
      webview: {
        options: {},
      },
      show: jest.fn(),

      visible: false,
    });

    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('show', () => {
    describe('when view exists', () => {
      describe('when view is visible', () => {
        it('does not call show', async () => {
          mockWebviewView = createFakePartial<vscode.WebviewView>({
            ...mockWebviewView,
            visible: true,
          });
          await controller.resolveWebviewView(mockWebviewView);

          await controller.show();

          expect(mockWebviewView.show).not.toHaveBeenCalled();
        });
      });

      describe('when view is not visible', () => {
        it('shows the view', async () => {
          await controller.resolveWebviewView(mockWebviewView);

          const showPromise = controller.show();
          controller.setChatReady();
          jest.advanceTimersByTime(10000);

          await showPromise;
          expect(mockWebviewView.show).toHaveBeenCalled();
        });
      });
    });

    describe('when view does not exist', () => {
      it('waits for view to load and executes focus command', async () => {
        const showPromise = controller.show();
        expect(mockExecuteCommand).toHaveBeenCalledWith('gl.webview.duo-chat-v2.focus');
        await jest.runAllTicks();
        controller.setChatReady();
        jest.advanceTimersByTime(10000);

        mockWebviewView = createFakePartial<vscode.WebviewView>({
          ...mockWebviewView,
          visible: true,
        });
        await controller.resolveWebviewView(mockWebviewView);

        await showPromise;
        expect(mockExecuteCommand).toHaveBeenCalledWith(USER_COMMANDS.FOCUS_CHAT);
      });
    });
  });

  describe('hide', () => {
    describe('when view is visible', () => {
      it('runs command to close sidebar', async () => {
        mockWebviewView = createFakePartial<vscode.WebviewView>({
          ...mockWebviewView,
          visible: true,
        });
        await controller.resolveWebviewView(mockWebviewView);

        await controller.hide();

        expect(mockExecuteCommand).toHaveBeenCalledWith('workbench.action.closeSidebar');
      });
    });

    describe('when view is not visible', () => {
      it('does not run command', async () => {
        await controller.hide();
        expect(mockExecuteCommand).not.toHaveBeenCalled();
      });
    });
  });

  describe('focusChat', () => {
    describe('when view is visible', () => {
      it('runs command to focus chat', async () => {
        mockWebviewView = createFakePartial<vscode.WebviewView>({
          ...mockWebviewView,
          visible: true,
        });
        await controller.resolveWebviewView(mockWebviewView);

        await controller.focusChat();

        expect(mockExecuteCommand).toHaveBeenCalledWith(USER_COMMANDS.FOCUS_CHAT);
      });
    });

    describe('when view is not visible', () => {
      it('does not run command', async () => {
        await controller.focusChat();
        expect(mockExecuteCommand).not.toHaveBeenCalled();
      });
    });
  });

  describe('#waitForChatReady', () => {
    it('resolves when chat is set ready within timeout', async () => {
      const showPromise = controller.show();
      await jest.runAllTicks();
      controller.setChatReady();
      jest.advanceTimersByTime(500);

      await expect(showPromise).resolves.toBeUndefined();
    });

    it('rejects if chat was not ready within timeout', async () => {
      const showPromise = controller.show(); // Starts the wait
      await jest.runAllTicks();
      jest.advanceTimersByTime(10000);
      await expect(showPromise).rejects.toThrow(`The webview didn't initialize in ${10000}ms`);
    });
  });
});
