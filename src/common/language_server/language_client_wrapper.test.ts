import vscode, { TabGroup } from 'vscode';
import {
  DidChangeDocumentInActiveEditor,
  TRACKING_EVENTS,
  AiContextEditorRequests,
  FeatureStateChangeNotificationType,
  FeatureState,
} from '@gitlab-org/gitlab-lsp';
import {
  BaseLanguageClient,
  DidChangeConfigurationNotification,
  GenericNotificationHandler,
  NotificationType,
} from 'vscode-languageclient';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { GitLabPlatformManagerForCodeSuggestions } from '../code_suggestions/gitlab_platform_manager_for_code_suggestions';
import { GitLabPlatformForAccount } from '../platform/gitlab_platform';
import { gitlabPlatformForAccount, gitlabPlatformForProject } from '../test_utils/entities';
import { setFakeWorkspaceConfiguration } from '../test_utils/vscode_fakes';
import { asMutable } from '../test_utils/types';
import { GitLabTelemetryEnvironment } from '../platform/gitlab_telemetry_environment';
import { log } from '../log';
import {
  getLocalFeatureFlagService,
  LocalFeatureFlagService,
} from '../feature_flags/local_feature_flag_service';
import { LSGitProvider } from '../git/ls_git_provider';
import {
  WebviewMessageRegistry,
  handleWebviewNotification,
  handleWebviewRequest,
  DEFAULT_WEBVIEW_NOTIFICATION_METHOD,
  DEFAULT_WEBVIEW_REQUEST_METHOD,
} from '../webview';
import { LanguageClientWrapper } from './language_client_wrapper';
import { FeatureStateManager } from './feature_state_manager';

jest.mock('../code_suggestions/gitlab_platform_manager_for_code_suggestions');
jest.mock('../log'); // disable logging in tests
jest.mock('../feature_flags/local_feature_flag_service');

jest.mock('../webview');

describe('LanguageClientWrapper', () => {
  let client: BaseLanguageClient;
  const getGitLabPlatformMock = jest.fn();
  const fakeManager = createFakePartial<GitLabPlatformManagerForCodeSuggestions>({
    getGitLabPlatform: getGitLabPlatformMock,
  });

  const gitLabTelemetryEnvironment = createFakePartial<GitLabTelemetryEnvironment>({
    isTelemetryEnabled: jest.fn(),
  });

  let lsGitProvider: LSGitProvider;

  let webviewMessageRegistry: WebviewMessageRegistry;

  let featureStateManager: FeatureStateManager;
  const createWrapper = (
    options: Partial<{
      mockClient: BaseLanguageClient;
      mockSuggestionsManager: GitLabPlatformManagerForCodeSuggestions;
      mockTelemetryEnvironment: GitLabTelemetryEnvironment;
      mockLsGitProvider: LSGitProvider;
      mockWebviewMessageRegistry: WebviewMessageRegistry;
      mockFeatureStateManager: FeatureStateManager;
    }> = {},
  ) => {
    const {
      mockClient = client,
      mockSuggestionsManager = fakeManager,
      mockTelemetryEnvironment = gitLabTelemetryEnvironment,
      mockLsGitProvider = lsGitProvider,
      mockWebviewMessageRegistry = webviewMessageRegistry,
      mockFeatureStateManager = featureStateManager,
    } = options;

    return new LanguageClientWrapper(
      mockClient,
      mockSuggestionsManager,
      mockTelemetryEnvironment,
      mockLsGitProvider,
      mockWebviewMessageRegistry,
      mockFeatureStateManager,
    );
  };

  beforeEach(() => {
    const gitLabPlatform: GitLabPlatformForAccount = gitlabPlatformForAccount;
    getGitLabPlatformMock.mockResolvedValue(gitLabPlatform);
    client = createFakePartial<BaseLanguageClient>({
      start: jest.fn(),
      stop: jest.fn(),
      registerProposedFeatures: jest.fn(),
      onNotification: jest.fn(),
      sendNotification: jest.fn(),
      onRequest: jest.fn(),
    });
    lsGitProvider = createFakePartial<LSGitProvider>({
      getDiffWithHead: jest.fn(),
      getDiffWithBranch: jest.fn(),
    });
    jest
      .mocked(getLocalFeatureFlagService)
      .mockReturnValue(createFakePartial<LocalFeatureFlagService>({ isEnabled: () => true }));

    webviewMessageRegistry = createFakePartial<WebviewMessageRegistry>({
      initNotifier: jest.fn(),
    });

    featureStateManager = createFakePartial<FeatureStateManager>({
      setStates: jest.fn(),
    });
  });

  describe('initAndStart', () => {
    it('starts the client and synchronizes the configuration', async () => {
      const wrapper = createWrapper();

      await wrapper.initAndStart();

      expect(client.registerProposedFeatures).toHaveBeenCalled();
      expect(client.start).toHaveBeenCalled();
      expect(client.sendNotification).toHaveBeenCalledWith(
        DidChangeConfigurationNotification.type,
        {
          settings: {
            baseUrl: gitlabPlatformForAccount.account.instanceUrl,
            projectPath: '',
            token: gitlabPlatformForAccount.account.token,
            telemetry: {
              actions: [{ action: TRACKING_EVENTS.ACCEPTED }],
            },
            featureFlags: {
              codeSuggestionsClientDirectToGateway: true,
              duoWorkflowBinary: true,
              remoteSecurityScans: true,
              streamCodeGenerations: true,
            },
            codeCompletion: {
              additionalLanguages: [],
              disabledSupportedLanguages: [],
            },
            securityScannerOptions: {
              enabled: false,
            },
            openTabsContext: true,
            suggestionsCache: undefined,
            logLevel: 'info',
            ignoreCertificateErrors: false,
            httpAgentOptions: {
              ca: undefined,
              cert: undefined,
              certKey: undefined,
            },
            duo: {
              enabledWithoutGitLabProject: undefined,
              workflow: {
                dockerSocket: '/var/run/docker.sock',
                useDocker: false,
              },
            },
          },
        },
      );
    });

    it('sends empty token when there is no account', async () => {
      getGitLabPlatformMock.mockResolvedValue(undefined);
      const wrapper = createWrapper();

      await wrapper.initAndStart();

      expect(client.sendNotification).toHaveBeenCalledWith(
        DidChangeConfigurationNotification.type,
        {
          settings: { token: '' },
        },
      );
    });

    it('stops client when disposed', async () => {
      const wrapper = createWrapper();

      await wrapper.initAndStart();
      wrapper.dispose();

      expect(client.stop).toHaveBeenCalled();
    });

    describe('sendOpenTabs', () => {
      describe('with no open tabs', () => {
        it('does not send and "textDocument/didOpen" events', async () => {
          const wrapper = createWrapper();

          await wrapper.initAndStart();

          expect(client.sendNotification).not.toHaveBeenCalledWith(
            expect.objectContaining({
              method: 'textDocument/didOpen',
            }),
            expect.anything(),
          );
        });
      });

      describe('with tab groups and open tabs', () => {
        beforeEach(() => {
          const mockTextDocuments = [
            createFakePartial<vscode.TextDocument>({
              uri: vscode.Uri.file('/path/to/file1.ts'),
              getText: () => '',
            }),
            createFakePartial<vscode.TextDocument>({
              uri: vscode.Uri.file('/path/to/file2.ts'),
              getText: () => '',
            }),
            createFakePartial<vscode.TextDocument>({
              uri: vscode.Uri.file('/path/to/file3.ts'),
              getText: () => '',
            }),
          ];

          jest.mocked(vscode.workspace.openTextDocument).mockImplementation(uri => {
            let textDocument: vscode.TextDocument | undefined;
            if (uri instanceof vscode.Uri) {
              textDocument = mockTextDocuments.find(doc => doc.uri.path === uri.path);
            } else if (typeof uri === 'string') {
              textDocument = mockTextDocuments.find(doc => doc.uri.path === uri);
            }
            return Promise.resolve(textDocument!);
          });

          asMutable(vscode.window.tabGroups).all = [
            createFakePartial<TabGroup>({
              tabs: [
                createFakePartial<vscode.Tab>({
                  input: new vscode.TabInputText(vscode.Uri.file('/path/to/file1.ts')),
                }),
                createFakePartial<vscode.Tab>({
                  input: new vscode.TabInputText(vscode.Uri.file('/path/to/file2.ts')),
                }),
              ],
            }),
            createFakePartial<TabGroup>({
              tabs: [
                createFakePartial<vscode.Tab>({
                  input: new vscode.TabInputText(vscode.Uri.file('/path/to/file3.ts')),
                }),
                createFakePartial<vscode.Tab>({
                  input: createFakePartial<vscode.TabInputTextDiff>({
                    original: '/path/to/file4.ts',
                  }),
                }),
              ],
            }),
          ];
        });

        afterEach(() => {
          asMutable(vscode.window.tabGroups).all = [];
        });

        it('logs an error when accessing tabs fails', async () => {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          asMutable(vscode.window.tabGroups).all = null as any; // Force an invalid vscode state

          const wrapper = createWrapper();

          await wrapper.initAndStart();

          expect(log.error).toHaveBeenCalledWith(
            'Failed to send existing open tabs to language server: ',
            expect.any(Error),
          );
        });

        it('logs a warning when sending a document fails', async () => {
          const error = new Error(`oh no!`);
          jest.mocked(vscode.workspace.openTextDocument).mockImplementation(uri => {
            if (uri instanceof vscode.Uri && uri.path === '/path/to/file1.ts') {
              throw error;
            }
            return Promise.resolve(createFakePartial<vscode.TextDocument>({}));
          });
          const wrapper = createWrapper();

          await wrapper.initAndStart();

          expect(log.warn).toHaveBeenCalledWith(
            'Failed to send "textDocument.didOpen" event for "file:///path/to/file1.ts"',
            error,
          );
        });

        it('sends "textDocument/didOpen" event for each open document', async () => {
          const wrapper = createWrapper();

          await wrapper.initAndStart();

          expect(client.sendNotification).toHaveBeenCalledWith(
            expect.objectContaining({
              method: 'textDocument/didOpen',
            }),
            { textDocument: expect.objectContaining({ uri: 'file:///path/to/file1.ts' }) },
          );
          expect(client.sendNotification).toHaveBeenCalledWith(
            expect.objectContaining({
              method: 'textDocument/didOpen',
            }),
            { textDocument: expect.objectContaining({ uri: 'file:///path/to/file2.ts' }) },
          );
          expect(client.sendNotification).toHaveBeenCalledWith(
            expect.objectContaining({
              method: 'textDocument/didOpen',
            }),
            { textDocument: expect.objectContaining({ uri: 'file:///path/to/file3.ts' }) },
          );
        });

        it('does not send "textDocument/didOpen" event for unsupported document types', async () => {
          const wrapper = createWrapper();

          await wrapper.initAndStart();

          expect(client.sendNotification).not.toHaveBeenCalledWith(
            expect.objectContaining({
              method: 'textDocument/didOpen',
            }),
            { textDocument: expect.objectContaining({ original: '/path/to/file4.ts' }) },
          );
        });
      });
    });

    describe('sendActiveDocument', () => {
      const activeEditorDocumentFileUri = 'file://file.js';

      beforeEach(async () => {
        vscode.window.activeTextEditor = createFakePartial<vscode.TextEditor>({
          document: { uri: activeEditorDocumentFileUri },
        });

        const wrapper = createWrapper();

        await wrapper.initAndStart();
      });

      it('send DidChangeDocumentInActiveEditor event', async () => {
        expect(client.sendNotification).toHaveBeenCalledWith(
          DidChangeDocumentInActiveEditor,
          activeEditorDocumentFileUri,
        );
      });
    });

    describe('inside a GitLab project', () => {
      beforeEach(() => {
        getGitLabPlatformMock.mockResolvedValue(gitlabPlatformForProject);

        client = createFakePartial<BaseLanguageClient>({
          start: jest.fn(),
          stop: jest.fn(),
          registerProposedFeatures: jest.fn(),
          onNotification: jest.fn(),
          onRequest: jest.fn(),
          sendNotification: jest.fn(),
        });
      });

      it('sends the project path', async () => {
        const wrapper = createWrapper();

        await wrapper.initAndStart();

        expect(client.sendNotification).toHaveBeenCalledWith(
          DidChangeConfigurationNotification.type,
          {
            settings: expect.objectContaining({
              projectPath: gitlabPlatformForProject.project?.namespaceWithPath,
            }),
          },
        );
      });
    });

    describe('registers webviews-related notifications', () => {
      it('registers handlers for DEFAULT_WEBVIEW_REQUEST_METHOD and DEFAULT_WEBVIEW_NOTIFICATION_METHOD', async () => {
        const mockRequestHandler = jest.fn();
        const mockNotificationHandler = jest.fn();

        jest.mocked(handleWebviewRequest).mockReturnValue(mockRequestHandler);
        jest.mocked(handleWebviewNotification).mockReturnValue(mockNotificationHandler);

        const wrapper = createWrapper();

        await wrapper.initAndStart();

        expect(client.onRequest).toHaveBeenCalledWith(
          DEFAULT_WEBVIEW_REQUEST_METHOD,
          mockRequestHandler,
        );
        expect(client.onNotification).toHaveBeenCalledWith(
          DEFAULT_WEBVIEW_NOTIFICATION_METHOD,
          mockNotificationHandler,
        );
      });

      it('initializes the webview message registry notifier', async () => {
        const wrapper = createWrapper();

        await wrapper.initAndStart();

        expect(webviewMessageRegistry.initNotifier).toHaveBeenCalledWith(expect.any(Function));

        const notifierFunction = jest.mocked(webviewMessageRegistry.initNotifier).mock.calls[0][0];
        const testMessage = { type: 'test', payload: 'data' };
        await notifierFunction(testMessage);

        expect(client.sendNotification).toHaveBeenCalledWith(
          DEFAULT_WEBVIEW_NOTIFICATION_METHOD,
          testMessage,
        );
      });
    });

    describe('Feature State Manager notification', () => {
      const notificationHandlers: Map<
        NotificationType<unknown> | string,
        GenericNotificationHandler
      > = new Map();

      let wrapper: LanguageClientWrapper;

      beforeEach(async () => {
        jest.mocked(client.onNotification).mockImplementation((type, handler) => {
          notificationHandlers.set(type, handler);
          return {
            dispose: () => {},
          };
        });

        wrapper = createWrapper();
        await wrapper.initAndStart();
      });

      it('sets feature manager states when receiving "FeatureStateChangeNotification"', async () => {
        const notificationPayload = createFakePartial<FeatureState[]>([]);
        notificationHandlers.get(FeatureStateChangeNotificationType)?.(notificationPayload);

        expect(featureStateManager.setStates).toHaveBeenLastCalledWith(notificationPayload);
      });
    });
  });

  describe('sendSuggestionAcceptedEvent', () => {
    it('sends accepted notification', async () => {
      const wrapper = createWrapper();

      // this is important step, we reference the function WITHOUT it's class instance
      // to test that we can pass it around as a command
      const { sendSuggestionAcceptedEvent } = wrapper;

      const trackingId = 'trackingId';
      const optionId = 1;

      await sendSuggestionAcceptedEvent(trackingId, optionId);

      expect(client.sendNotification).toHaveBeenCalledWith('$/gitlab/telemetry', {
        category: 'code_suggestions',
        action: TRACKING_EVENTS.ACCEPTED,
        context: { trackingId, optionId },
      });
    });
  });

  describe('syncConfig', () => {
    let subject: LanguageClientWrapper;

    beforeEach(async () => {
      subject = createWrapper();
      await subject.initAndStart();

      // initAndStart() will trigger some mocks, so let's start from a clean slate
      jest.clearAllMocks();
    });

    it('reads featureFlags configuration', async () => {
      jest
        .mocked(getLocalFeatureFlagService)
        .mockReturnValue(createFakePartial<LocalFeatureFlagService>({ isEnabled: () => false }));

      await subject.syncConfig();

      expect(client.sendNotification).toHaveBeenCalledTimes(1);
      expect(client.sendNotification).toHaveBeenCalledWith(
        DidChangeConfigurationNotification.type,
        {
          settings: expect.objectContaining({
            featureFlags: {
              codeSuggestionsClientDirectToGateway: false,
              duoWorkflowBinary: false,
              remoteSecurityScans: false,
              streamCodeGenerations: false,
            },
          }),
        },
      );
    });

    describe('syncs telemetry configuration', () => {
      it.each([true, false])('when telemetry enabled is set to %s', async isTelemetryEnabled => {
        jest
          .mocked(gitLabTelemetryEnvironment.isTelemetryEnabled)
          .mockReturnValueOnce(isTelemetryEnabled);

        await subject.syncConfig();

        expect(client.sendNotification).toHaveBeenCalledWith(
          DidChangeConfigurationNotification.type,
          {
            settings: expect.objectContaining({
              telemetry: expect.objectContaining({
                enabled: isTelemetryEnabled,
              }),
            }),
          },
        );
      });
    });

    describe.each`
      configured                                | expected
      ${{ ignoreCertificateErrors: false }}     | ${{ ignoreCertificateErrors: false }}
      ${{ ignoreCertificateErrors: null }}      | ${{ ignoreCertificateErrors: false }}
      ${{ ignoreCertificateErrors: true }}      | ${{ ignoreCertificateErrors: true }}
      ${{ ignoreCertificateErrors: undefined }} | ${{ ignoreCertificateErrors: false }}
      ${{ ca: 'test-ca' }}                      | ${{ httpAgentOptions: { ca: 'test-ca' } }}
      ${{ cert: 'test-cert' }}                  | ${{ httpAgentOptions: { cert: 'test-cert' } }}
      ${{ certKey: 'test-certKey' }}            | ${{ httpAgentOptions: { certKey: 'test-certKey' } }}
    `('$expected when workspace included $configured', ({ configured, expected }) => {
      it('should send a configuration changed notification', async () => {
        setFakeWorkspaceConfiguration(configured);

        await subject.syncConfig();

        expect(client.sendNotification).toHaveBeenCalledWith(
          DidChangeConfigurationNotification.type,
          {
            settings: expect.objectContaining(expected),
          },
        );
      });
    });
  });

  describe('git diff request handling', () => {
    let requestHandlers: Record<string, (...args: unknown[]) => unknown>;
    let wrapper: LanguageClientWrapper;

    beforeEach(async () => {
      requestHandlers = {};
      jest.mocked(client.onRequest).mockImplementation((type, handler) => {
        requestHandlers[type] = handler;
        return {
          dispose: () => {},
        };
      });

      wrapper = createWrapper();
      await wrapper.initAndStart();
    });

    it('registers git diff request handler', () => {
      expect(client.onRequest).toHaveBeenCalledWith(
        AiContextEditorRequests.GIT_DIFF,
        expect.any(Function),
      );
    });

    describe('when requesting diff with HEAD', () => {
      const repositoryUri = 'file:///path/to/repo';
      const expectedDiff = 'diff --git a/file.txt b/file.txt';

      beforeEach(() => {
        jest.mocked(lsGitProvider.getDiffWithHead).mockResolvedValue(expectedDiff);
      });

      it('returns diff from LSGitProvider', async () => {
        const result = await requestHandlers[AiContextEditorRequests.GIT_DIFF]({
          repositoryUri,
        });

        expect(result).toBe(expectedDiff);
        expect(lsGitProvider.getDiffWithHead).toHaveBeenCalledWith(vscode.Uri.parse(repositoryUri));
      });
    });

    describe('when requesting diff with specific branch', () => {
      const repositoryUri = 'file:///path/to/repo';
      const branch = 'feature-branch';
      const expectedDiff = 'diff --git a/file.txt b/file.txt';

      beforeEach(() => {
        jest.mocked(lsGitProvider.getDiffWithBranch).mockResolvedValue(expectedDiff);
      });

      it('returns diff from LSGitProvider', async () => {
        const result = await requestHandlers[AiContextEditorRequests.GIT_DIFF]({
          repositoryUri,
          branch,
        });

        expect(result).toBe(expectedDiff);
        expect(lsGitProvider.getDiffWithBranch).toHaveBeenCalledWith(
          vscode.Uri.parse(repositoryUri),
          branch,
        );
      });
    });
  });
});
