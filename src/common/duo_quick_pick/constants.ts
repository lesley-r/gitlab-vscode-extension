export const CODE_SUGGESTIONS_ENABLED =
  '$(gitlab-code-suggestions-enabled) GitLab Duo Code Suggestions: Enabled';
export const CODE_SUGGESTIONS_DISABLED =
  '$(gitlab-code-suggestions-disabled) GitLab Duo Code Suggestions: Disabled';
export const CODE_SUGGESTIONS_DESCRIPTION = 'Code completion + generation';
export const DUO_UNAVAILABLE = '$(gitlab-code-suggestions-disabled) Duo unavailable';
export const DUO_CHAT_ENABLED = '$(gitlab-duo-chat-enabled) GitLab Duo Chat: Enabled';
export const DUO_CHAT_DISABLED = '$(gitlab-duo-chat-disabled) GitLab Duo Chat: Disabled';
export const NOT_AUTHENTICATED = 'Please sign in to GitLab';
export const ENABLE_CODE_SUGGESTIONS = 'Enable Code Suggestions';
export const DISABLE_CODE_SUGGESTIONS = 'Disable Code Suggestions';
export const DUO_SETTINGS = 'Duo Settings';
export const DOCUMENTATION = 'Documentation';
export const GITLAB_FORUM = 'GitLab Forum';
export const GITLAB_FORUM_DESCRIPTION = 'Help and feedback';

export const DOCUMENTATION_URL = 'https://docs.gitlab.com/ee/user/gitlab_duo/';
export const GITLAB_FORUM_URL = 'https://forum.gitlab.com/c/gitlab-duo/52';

export const NO_ACTIVE_EDITOR_NOTIFICATION =
  'No active editor. Open a file to use Duo Code Suggestions.';
export const CODE_SUGGESTIONS_DISABLED_NOTIFICATION =
  'GitLab Duo Code Suggestions is currently disabled.';
export const DUO_CHAT_DISABLED_NOTIFICATION = 'GitLab Duo Chat is currently disabled.';

export const GITLAB_WORKFLOW_SETTINGS_ANCHOR = '@ext:gitlab.gitlab-workflow';

export const DUO_STATUS_ZERO_PROBLEMS_DETECTED = 'Status: No problems detected';
