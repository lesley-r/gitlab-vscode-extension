import { browser } from '@wdio/globals';

/**
 * Verifies Duo Workflow run button
 *
 * @async
 * @returns {Promise<void>}
 */
const verifyDuoWorkflowStartButton = async () => {
  await browser.waitUntil(
    async () => {
      await browser.$$('btn.btn-confirm.btn-md.gl-button');
    },
    {
      timeout: 10000,
      timeoutMsg: `Start Workflow button didn't appear.`,
    },
  );
};

export { verifyDuoWorkflowStartButton };
